#!/usr/bin/python3.10

# Ce fichier, qui fait partie du merge zcasino_v0
# est ma recherche sur le projet. Ainsi que les aqmélioration
# Pour voir la vraie correction, ouvrir le fichier avec le code Web

# Le projet zcasino
# jrd10 Avril 2023
# /home/jrd_10/Documents/FLOSS/Python/GitLab-python/apprenez-python-4-edition/
#   projects/zcasino/zcasino_v0.1.py

# Dans le livre, page 93, Code web 366476 (ne fonctionne plus)
# Voir sur Gillab : https://gitlab.com/jrd10/apprenez-python-4-edition

# Un jeu de roulette
# Numéro entre 0 et 49
# Somme possédée et dépôt de somme sur le Numéro
# Noires : impaires et Rouges : impaires
# Si numéro gagnant : 3 x la somme misée
# Si même couleur, 50% de la mise
# Le jeu s'arrête que la joueur n'a plus d'argent


# Variables

# depot_joueur : float, 2 décimal, en euro
## Amélioration : poser la question du dépôt
## Amélioration : dans une autre monnaie avec conversion automatique en ligne

# numero_joueur : (0 à 49)
# mise_joueur : float, 2 décimal, en euro
## Amélioration : multijoueur, quitter le jeu

# numero_partie : int(0, 50)
# couleur_partie : noire : int(numero_partie) % 2 <> 0 else noire

# solde_joueur : depot_joueur - gain (positif ou négatif)

# gain_numero : mise_joueur * 3
# gain_couleur : mise_joueur / 2

## Dépôt mise_joueur

### Variables

from random import randint
from math   import ceil


argent = 500
continuer_partie = True

print(f"Vous vous êtes installé à une table avec {argent} €.")

while continuer_partie :
    nombre_mise =-1

    while nombre_mise < 0:
        nombre_mise = input("Taper le numéro de votre choix (de 0 à 49) : ")
        nombre_mise = int(nombre_mise)

    mise = -1

    while mise <= 0 or mise > argent:
        mise = input("Montant de votre mise : ")
        mise = int(mise)


    numero_gagnant = randint(0, 49)

    print(f"La roulette tourne et le numéro gagnant est le {numero_gagnant}.")

    if numero_gagnant == nombre_mise :
        argent += mise
        print(f"Félicitations, vous avez gagné : {mise * 3}. Votre solde : {argent}\n.")



    elif numero_gagnant % 2 == nombre_mise % 2:
        mise = ceil(mise * 0.5)
        argent += mise

        print(f"Vous avez gagné : {mise}. Votre solde : {argent}.\n")



    else:
        argent -= mise

        print(f"Désolé, vous avez perdu. Votre solde : {argent}.\n")



    if argent <= 0:
        print("Vous êtes ruiné")

        match input("Souhaitez-vous quitter la partie (o/n) ?"):

            case "O" | "o" :
                print("Vous avez quitté la partie avec vos gains")

                continuer_partie = false

#!/usr/bin/python3.10

# Le projet zcasino Solution Code Web 366476
# jrd10 Avril 2023
# /home/jrd_10/Documents/FLOSS/Python/GitLab-python/apprenez-python-4-edition/
#   projects/zcasino/CodeWeb366476_zcasino.py

# Dans le livre, page 93, le lien Code web 366476 (ne fonctionne plus)
# Voir sur Gillab : https://gitlab.com/jrd10/apprenez-python-4-edition

# Un jeu de roulette
# Numéro entre 0 et 49
# Somme possédée et dépôt de somme sur le Numéro
# Noires : impaires et Rouges : impaires
# Si numéro gagnant : 3 x la somme misée
# Si même couleur, 50% de la mise
# Le jeu s'arrête que la joueur n'a plus d'argent


"Ce fichier abrite le code de zcasino, un jeu de roulette adapté"

import os   # pour Windows
from random import randint
from math   import ceil


# Déclaration des variables de départ
argent = 1000 # On a mille euros au début du jeu
continuer_partie = True     # Booléen qui est vrai tant que l'on doit
                            # continuer la partie

print(f"Vous vous installez à la table de roulette avec {argent} euro (s).")

while continuer_partie : # Tant que l'on doit continuer la continuer_partie
            # On demande à l'utilisateur de saisir le nombre utilisateur
            # lequel il va miser
    nombre_misé =-1

    while nombre_misé < 0:
        nombre_misé = input("Tapez le numéro sur lequel vous voulez miser (entre 0 et 49) : ")

        # On convertit le nombre misé
        try:
            nombre_misé = int(nombre_misé)
            if nombre_misé < 0 or nombre_misé > 49:
                raise ValueError

        except ValueError:
            print("Vous n'avez pas saisie de nombre valide.")
            nombre_misé = -1

    # À présent on sélectionne la somme à miser sur le nombre_mise
    mise = -1
    while mise <=0 or mise >= argent:
        mise = input("Tapez le montant de votre mise : ")

        # On convertit la miser
        try:
            mise =  int(mise)
            if mise <=0 or mise > argent:
                raise ValueError

        except ValueError:
            print("Vous n'avez pas saisie de nombre valide.")
            mise = -1

    # Le nombre misé et la mise ont été sélectionns par
    # l'utilisateur. On fait tourner la roulette.

    numéro_gagnant = randint(0, 49)
    print(f"La roulette tourne... ... et s'arrête sur le numéro gagnant {numéro_gagnant}.")

    # On établit le gain du joueur
    if numéro_gagnant == nombre_misé:
        print(f"Félicitations ! Vous obtenez {mise * 3} euro(s) !")
        argent += mise

    elif numéro_gagnant % 2 == nombre_misé % 2:  # Ils sont de la
                                                # même couleur
        mise = ceil(mise * 0.5)
        print(f"Vous avez misé la bonne couleur. Vous obtenez {mise} euro(s).")
        argent += mise
        
    else:
        print("Désolé l'ami, ce n'est pas pour cette fois. Vous perdez votre mise")
        argent -= mise


    # On interrompt le jeu si le joueur est ruiné
    if argent <= 0:
        print("Vous êtes ruiné ! C'est la fin de la partie.")
        continuer_partie = false

    else:
        # On affiche l'argent du joueur
        print(f"Vous avez à présent {argent} euro(s).")

        match input("Souhaitez-vous quitter le casino avec vos gains ?"):
                # l'utilisateur entre `o` ou `O`
                case "o" | "O":
                    print("Vous quittez le casino avec vos gains.")

                    continuer_partie = False

# On met en pause le système (Windows)
os.system("pause")

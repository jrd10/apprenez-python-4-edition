# Apprenez à programmer en Python, 4e édition, de Vincent Le Goff


## Origine de ce projet

Bonjour,

Je me "remets" à Python ! Même si je n'ai jamais été un vrai développeur :). Je suis retraité.

J'ai fait l'acquisition du livre de **Vincent Le Goff**, **APPRENEZ à PROGRAMMER EN PYTHON**. Oui, c'est écrit en majuscules :).
c'est la 4e édition du livre. Le livre est basé sur Python 3.10.

Au moment où je rédige ces lignes, mars 2023, Python est en `Latest: Python 3.11.2`

Note : je suis sur Ubuntu 22.04.2, est la version installée est (un p'tit coup de terminal) :

```
jrd_10@my-pc:~$ python --version
Python 3.10.6
```

Je me propose de partager mes exercices ici :)

Que vous pourrez forker, améliorer et, si nécessaire, corriger :).

## Lien avec Le site du Zéro

Il semble que ce livre a été écrit en lien avec l'ancien Site du Zéro. Aujourd'hui, OPENCLASSROOMS (https://openclassrooms.com).

Les codewebs, les pragrammes en ligne, ne semblent plus accessibles.

J'ai posé la question sur le forum de OPENCLASSROOMS, voici le fil : https://openclassrooms.com/forum/sujet/codewebs-inacessibles-livre-le-goff-python#message-94890645. Merci à `AbcAbc6` du retour.


## Partage de la progression et du code

Dans ce présent projet (si on peut appeler cela un projet), je compte :

- Partager ma progression... c'est aussi une motivation pour moi :)

- Partager mes bouts de code :)

Bonne lecture et bonne inspiration. N'hésitez pas à partager des idées.


## Licence
Pas de licence... domaine public.

## Project status

- Je viens juste de démarrer en mars 2023.

- Il y a... 42 chapitres. Dont certains assez avancés.
Je me donne comme objectif de la terminer pour septembre (je ne donne pas l'année :).

- Si vous le souhaitez, vous pouvez rejoindre le "projet" :).

- Git installée. Projet clôné sur mon PC :) dans le répertoire "Gitlab-python".

Amicalement,

José Relland - jrd10
_le 30 mars 2023, Les Rousses._

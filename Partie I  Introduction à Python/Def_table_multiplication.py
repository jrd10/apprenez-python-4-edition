# Table de multiplication avec une fonction
# jrd10 Mars 2023
# /home/jrd_10/Documents/FLOSS/Python/Apprenez Python/Def_table_multiplication.py

# Saisir une table table
# Dérouler de 1 à 10


# Fonction affichage de la table
def table(table_choix):
    """ Table de multiplication

    Arguments : la table recherchée
    """

    for i in range(1, 11):
        # print(f"{table_choix} x {i} = ", table_choix * i)
        print(f"{table_choix} x {i} = {table_choix * i}.")

# Saisir la table
table_choix = input("Saisir la table (de 1 à 10) : ")
print("---")
print(f"Vous avez choisi la table des : {table_choix}.")
print("---")
table_choix = int(table_choix)

# appel de la fonction
table(table_choix)

print("---")

# Saisir la table
table_choix = input("Allez, un autre essai (de 1 à 10) : ")
print("---")
print(f"Vous avez choisi la table des : {table_choix}.")
print("---")
table_choix = int(table_choix)

# appel de la fonction
table(table_choix)

print("\nÀ savoir par cœur !\n")

print(help(table))

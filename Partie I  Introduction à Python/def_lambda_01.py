# Function lambda avec une puissance
# jrd10 Mars 2023
# /home/jrd_10/Documents/FLOSS/Python/Apprenez Python/def_lambda_01.py

# fonction puissance simplifiée avec lambda
#


# Fonction
f = lambda x, y: x+y

# données,
x = input("Saisissez la variable : ")
y = input("Saisissez la puissance : ")

# Retour le résultat comme une fonction
print(f"Résultat : {f(x, y)}.")

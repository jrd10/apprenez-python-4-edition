# Année bissextile
# jrd10 Mars 2023 : Solution Livre n° 2
# //home/jrd_10/Documents/FLOSS/Python/Apprenez Python/Bissextile_solution_livre_2.py

# programme testant si une année, saisie par l'utilisateur
# est bissextile ou Détermination
# Programme optimisé

année = input("Saisissez une année : ")

année = int(année)

if année % 400 == 0 or (année % 4 == 0 and année % 100 != 0):
    print(f"L'année {année} est bissextile.")

else:
    print(f"L'année {année} n'est pas bissextile.")

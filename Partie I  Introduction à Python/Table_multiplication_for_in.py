# Table de multiplication avec for et in
# jrd10 Mars 2023
# /home/jrd_10/Documents/FLOSS/Python/Apprenez Python/Table_multiplication_for_in.py

# Saisir une table table
# Dérouler de 1 à 10

# Remarque : ici, les bornes de l'itération ne sont pas connue

# Saisir la table
table_choix = input("Saisir la table (de 1 à 10) : ")
print("---")
print(f"Vous avez choisi la table des : {table_choix}.")
print("---")
table_choix = int(table_choix)

for i in range(1, 11):
    # print(f"{table_choix} x {i} = ", table_choix * i)
    print(f"{table_choix} x {i} = {table_choix * i}.") # on peux calculer dans les accolades :).


print("---")
print("Bravo !")

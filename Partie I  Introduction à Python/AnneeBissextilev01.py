/usr/local/bin/python3.10

# Année bissextile
# jrd10 Mars 2023
# File on Gitlab. Mars 2023
#/home/jrd_10/Documents/FLOSS/Python/Apprenez Python

# Divisible par 4
# Sauf si divisible par 100
# Divisible par 400

## Saisie de l'année

anneeSaisie = input("Entrée une année : ") # à retourner en int(anneeSaisie)

print(f"Année saisie : {anneeSaisie}.")


## Détermination des années bissextiles

anneeSaisie = int(anneeSaisie)

if anneeSaisie % 4 == 0 :
    if anneeSaisie % 100 == 0 :
        if anneeSaisie % 400 == 0 :
            print(f"L'année {anneeSaisie} est une année bissextile.")

else:
    print(f"L'année {anneeSaisie} n'est pas une année bissextile.")

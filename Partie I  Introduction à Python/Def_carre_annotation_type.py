# Function carré avec définition de type
# jrd10 Mars 2023
# /home/jrd_10/Documents/FLOSS/Python/Apprenez Python/Def_carre_annotation_type.py

# Carré du valeur int ou float
#


# Fonction affichage de la table
def carre(val: int | float) -> int | float:

    return val * val


# Saisir la valeur. Choisissez le type et enregistrer

# val = 8 # pour input
val = 8.5 # pour float


# val = input("Saisir un nombre à élever au carré : ")
# Ici, il faut tester le type et retourner celui qui correspond

print("---")


print(f"Le nombre {val} ({type(val)}) élevé au carré = {carre(val)} ({type(carre(val))}).")
print("---")

# print(f"Vous avez choisi le nombre : {table_choix}.")
#
#
# # appel de la fonction
# table(table_choix)
#
# print("---")
#
# # Saisir la table
# table_choix = input("Allez, un autre essai (de 1 à 10) : ")
# print("---")
# print(f"Vous avez choisi la table des : {table_choix}.")
# print("---")
# table_choix = int(table_choix)
#
# # appel de la fonction
# table(table_choix)
#
# print("\nÀ savoir par cœur !\n")
#
# print(help(table))

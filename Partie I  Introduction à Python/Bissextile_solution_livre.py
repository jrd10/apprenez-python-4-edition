# Année bissextile
# jrd10 Mars 2023 : Solution Livre
# /home/jrd_10/Documents/FLOSS/Python/Apprenez Python/Bissextile_solution_livre

# programme testant si une année, saisie par l'utilisateur
# est bissextile ou Détermination

année = input("Saisissez une année : ")

année = int(année)

bissextile = False

if année % 400 == 0:
  bissextile = True

elif année % 100 == 0:
  bissextile = False

elif année % 4 == 0:
  bissextile = True

else:
  bissextile = False


if bissextile: # test si vrai
  print(f"L'année {année} est bissextile")

else:
  print(f"L'année {année} n'est pas bissextile")

#!/usr/bin/python3.10

# Table de multiplication avec WHILE
# jrd10 Mars 2023
# /home/jrd_10/Documents/FLOSS/Python/Apprenez Python/table_multiplication.py

# Saisir une table table
# Dérouler de 1 à 10


# Saisir la table
table_choix = input("Saisir la table (de 1 à 10) : ")
print("---")
print(f"Vous avez choisi la table des : {table_choix}.")
print("---")
table_choix = int(table_choix)

i = 1
while i <= 12:
    # print(f"{table_choix} x {i} = ", table_choix * i)
    print(f"{table_choix} x {i} = {table_choix * i}.") # on peux calculer dans les accolades :).

    i += 1

print("---")
print("Bravo !")
